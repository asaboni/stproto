from fastapi import FastAPI
from starlette.testclient import TestClient
import pytest

from proto.api.model import model_router

app = FastAPI()
app.include_router(model_router)


@pytest.fixture()
def test_client():
    client = TestClient(app)
    return client


def extract_uri_from_response(response):
    pass


def test_model_hello_route_returns_correct_greeting(test_client):
    # Given
    expected_response = {
        'message': 'Hello_world'
    }

    # When
    response = test_client.get("/model/hello")

    # Then
    assert response.status_code == 200
    assert response.json() == expected_response


def test_model_excel_to_owl_takes_data_source_as_param_and_return_xml(test_client):
    # Given
    data_source = "stdf_ews"
    payload = {
        "data_source": data_source
    }

    # When
    response = test_client.get("/model/excel_to_owl", params=payload)

    # Then
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/xml'


# def test_model_excel_to_owl_takes_data_source_as_param_and_return_correct_data_source_uri(test_client):
#     # Given
#     data_source = "stdf_ews"
#     payload = {
#         "data_source": data_source
#     }
#     expected_uri = 'http://st.com/stdf.owl'
#
#     # When
#     response = test_client.get("/model/excel_to_owl", params=payload)
#
#     actual_uri = extract_uri_from_response(response)
#     # Then
#     assert actual_uri == expected_uri
