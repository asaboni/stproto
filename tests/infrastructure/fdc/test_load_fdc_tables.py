from proto.infrastructure.measurement.fdc.load_fdc_tables import load_table_from_dump_name, sanitize_limits_value_column

import numpy as np
import pandas as pd


def test_load_indicators_table_from_dump_name_returns_correct_table_for_tablename_and_header():
    # Given
    expected_indicator_table_header = ['indicator_id', 'strategy_id', 'label',
                                       'ltrs', 'ltri', 'filter_id', 'initial_target']
    test_indicator_table_tablename = 'PCB.Indicators.dump'
    expected_indicator_table_types = {
        'indicator_id': np.int64,
        'strategy_id': str,
        'label': str,
        'ltrs': str,
        'ltri': str,
        'filter_id': np.int64,
        'initial_target': np.float64
    }
    indicator_METADATA = {
        'filename': test_indicator_table_tablename,
        'names': expected_indicator_table_header,
        'types': expected_indicator_table_types
    }

    # When
    indicators_table = load_table_from_dump_name(indicator_METADATA)
    # Then
    actual_table_header = indicators_table.columns
    diff = set(actual_table_header) ^ set(expected_indicator_table_header)

    assert not diff
    assert len(set(actual_table_header)) == len(actual_table_header)


def test_load_treated_data_history_table_from_dump_name_returns_correct_table_for_tablename_and_header():
    # Given
    expected_treated_data_history_table_header = ['context_id', 'indicator_id', 'value']

    expected_treated_data_history_table_types = {
        'context_id': np.int64,
        'indicator_id': np.int64,
        'value': np.float64
    }
    test_treated_data_history_table_tablename = 'PCB.TreatedDataHistory.dump'
    treated_data_history_METADATA = {
        'filename': test_treated_data_history_table_tablename,
        'names': expected_treated_data_history_table_header,
        'types': expected_treated_data_history_table_types
    }

    # When
    actual_treated_data_history_table = load_table_from_dump_name(treated_data_history_METADATA)

    # Then
    actual_treated_data_history_table_header = actual_treated_data_history_table.columns
    diff = set(actual_treated_data_history_table_header) ^ set(expected_treated_data_history_table_header)

    assert not diff
    assert len(set(actual_treated_data_history_table_header)) == len(actual_treated_data_history_table_header)


def test_load_strategies_table_from_dump_name_returns_correct_table_for_tablename_and_header():
    # Given
    expected_strategies_table_header = ['strategy_id', 'label', 'dcqualitylevel', 'moduleid',
                                        'dceventstemplateid', 'dceventsconfigid']

    expected_strategies_table_types = {
        'strategy_id': str,
        'label': str,
        'dcqualitylevel': np.float64,
        'moduleid': str,
        'dceventstemplateid': str,
        'dceventsconfigid': np.int64
    }
    test_strategies_table_tablename = 'PCB.Strategies.dump'
    strategies_METADATA = {
        'filename': test_strategies_table_tablename,
        'names': expected_strategies_table_header,
        'types': expected_strategies_table_types
    }

    # When
    actual_strategies_table = load_table_from_dump_name(strategies_METADATA)

    # Then
    actual_strategies_table_header = actual_strategies_table.columns
    diff = set(actual_strategies_table_header) ^ set(expected_strategies_table_header)

    assert not diff
    assert len(set(actual_strategies_table_header)) == len(actual_strategies_table_header)


def test_load_limits_table_from_dump_name_returns_correct_table_for_tablename_and_header():
    # Given
    expected_limits_table_header = ['limit_id', 'indicator_id', 'label', 'value', 'alarm_level']

    expected_limits_table_types = {
        'limit_id': np.int64,
        'indicator_id': np.int64,
        'label': str,
        'value': np.float64,
        'alarm_level': np.int64
    }
    test_limits_table_tablename = 'PCB.Limits.dump'
    limits_METADATA = {
        'filename': test_limits_table_tablename,
        'names': expected_limits_table_header,
        'types': expected_limits_table_types
    }

    # When
    actual_limits_table = load_table_from_dump_name(limits_METADATA)

    # Then
    actual_limits_table_header = actual_limits_table.columns
    diff = set(actual_limits_table_header) ^ set(expected_limits_table_header)

    assert not diff
    assert len(set(actual_limits_table_header)) == len(actual_limits_table_header)


def test_load_mes_context_definition_table_from_dump_name_returns_correct_table_for_tablename_and_header_with_types():
    # Given
    expected_mes_context_definition_table_header = ['mes_context_id', 'technology_name', 'product_name',
                                                    'lot_name', 'wafer_name', 'route_name', 'stage_name',
                                                    'operation_name',
                                                    'aux1', 'aux2']
    expected_context_definition_table_types = {
        'mes_context_id': np.float64,
        'technology_name': str,
        'product_name': str,
        'lot_name': str,
        'wafer_name': str,
        'route_name': str,
        'stage_name': str,
        'operation_name': str,
        'aux1': str,
        'aux2': str
    }

    test_indicator_table_tablename = 'PCB.MESContextHistory.dump'
    context_definition_METADATA = {
        'filename': test_indicator_table_tablename,
        'names': expected_mes_context_definition_table_header,
        'types': expected_context_definition_table_types,
        'null_value': 'N.A.'
    }

    # When
    indicators_table = load_table_from_dump_name(context_definition_METADATA)
    # Then
    actual_table_header = indicators_table.columns
    diff = set(actual_table_header) ^ set(expected_mes_context_definition_table_header)

    assert not diff
    assert len(set(actual_table_header)) == len(actual_table_header)


def test_load_mes_association_table_from_dump_name_returns_correct_table_for_tablename_and_header_with_types():
    # Given
    expected_mes_association_table_table_header = ['context_id', 'mes_context_id']
    expected_mes_association_table_table_types = {
        'context_id': np.int64,
        'mes_context_id': np.int64,
    }

    test_indicator_table_tablename = 'PCB.MESFDCAssociation.dump'
    mes_association_table_METADATA = {
        'filename': test_indicator_table_tablename,
        'names': expected_mes_association_table_table_header,
        'types': expected_mes_association_table_table_types,
        'null_value': 'N.A.'
    }

    # When
    indicators_table = load_table_from_dump_name(mes_association_table_METADATA)
    # Then
    actual_table_header = indicators_table.columns
    diff = set(actual_table_header) ^ set(expected_mes_association_table_table_header)

    assert not diff
    assert len(set(actual_table_header)) == len(actual_table_header)


def test_load_context_history_table_from_dump_name_returns_correct_table_for_tablename_and_header_with_types():
    # Given
    expected_context_history_table_header = ['context_id', 'material_id', 'recipe_id', 'start_recipe_step',
                                             'equipment_id', 'module_id', 'Strategy_name', 'Strategy_id',
                                             'steady_event_time', 'start_event_time', 'stop_event_time',
                                             'dc_quality_value', 'completed', 'is_reference', 'is_archived',
                                             'archive_file', 'history_id']

    expected_context_history_table_types = {
        'context_id': np.float64,
        'material_id': str,
        'recipe_id': str,
        'start_recipe_step': np.int64,
        'equipment_id': str,
        'module_id': str,
        'Strategy_name': str,
        'Strategy_id': str,
        'steady_event_time': str,
        'start_event_time': str,
        'stop_event_time': str,
        'dc_quality_value': np.float64,
        'completed': str,
        'is_reference': str,
        'is_archived': str,
        'archived_file': str,
        'history_id': np.int64
    }

    test_indicator_table_tablename = 'PCB.ContextHistory.dump'
    context_history_METADATA = {
        'filename': test_indicator_table_tablename,
        'names': expected_context_history_table_header,
        'types': expected_context_history_table_types,
        'null_value': '',
        'dates': ['steady_event_time', 'start_event_time', 'stop_event_time']
    }

    # When
    indicators_table = load_table_from_dump_name(context_history_METADATA)
    # Then
    actual_table_header = indicators_table.columns
    diff = set(actual_table_header) ^ set(expected_context_history_table_header)

    assert not diff
    assert len(set(actual_table_header)) == len(expected_context_history_table_header)


def test_load_collected_data_history_table_from_dump_name_returns_correct_table_for_tablename_and_header_with_types():
    # Given
    expected_collected_data_history_table_header = ['context_id', 'variable_id', 'time_stamp', 'time_stamp_ms', 'value']
    expected_collected_data_history_table_types = {
        'context_id': np.float64,
        'variable_id': np.int64,
        'time_stamp': str,
        'time_stamp_ms': str,
        'value': np.float64
    }
    test_collected_data_history_table_tablename = 'PCB.CollectedDataHistory_0.dump'
    collected_data_history_METADATA = {
        'filename': test_collected_data_history_table_tablename,
        'names': expected_collected_data_history_table_header,
        'types': expected_collected_data_history_table_types
    }

    # When
    collected_data_history_table = load_table_from_dump_name(collected_data_history_METADATA)
    # Then
    actual_collected_data_history_table_header = collected_data_history_table.columns
    diff = set(actual_collected_data_history_table_header) ^ set(expected_collected_data_history_table_header)

    assert not diff
    assert len(set(actual_collected_data_history_table_header)) == len(expected_collected_data_history_table_header)


def test_load_variables_table_from_dump_name_returns_correct_table_for_tablename_and_header_with_types():
    # Given
    expected_variables_table_header = ['variable_id', 'label', 'unit', 'min_value', 'max_value', 'alias']
    expected_variables_table_types = {
        'variable_id': np.int64,
        'label': str,
        'unit': str,
        'min_value': np.float64,
        'max_value': np.float64,
        'alias': str
    }

    test_variables_table_tablename = 'PCB.Variables.dump'
    variables_METADATA = {
        'filename': test_variables_table_tablename,
        'names': expected_variables_table_header,
        'types': expected_variables_table_types
    }

    # When
    actual_variables_table = load_table_from_dump_name(variables_METADATA)
    actual_collected_data_history_table_header = actual_variables_table.columns

    # Then
    diff = set(actual_collected_data_history_table_header) ^ set(expected_variables_table_header)

    assert not diff
    assert len(set(actual_collected_data_history_table_header)) == len(expected_variables_table_header)


def test_sanitize_limit_value_column_returns_float_np_type():
    # Given
    test_alarm_levels = [-3, -3, -3]
    test_limit_values = ['6.0000000E+1', '2.9000000E+3', '5.8000000E+1']
    df_table = pd.DataFrame({
        'value': test_limit_values,
        'alarm_level': test_alarm_levels
    })

    # When
    sanitized_values_column = sanitize_limits_value_column(df_table['value'])

    # Then
    assert sanitized_values_column.dtype == np.float64
