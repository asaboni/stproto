import os

from proto.infrastructure.model.load_model import load_functional_flow_from_excel_file


def test_load_functional_flow_from_excel_file_returns_table_with_class_instance_column():
    # Given
    expected_model_ingestion_column = 'Class_Instance'
    pathname = os.path.dirname(__file__)
    filename = '2021-02-17-STDF_FUNCTIONAL_FLOW_RDM_V1.xlsx'
    excel_path = os.path.join(pathname, filename)

    # When
    actual_table_columns = load_functional_flow_from_excel_file(excel_path).columns

    # Then
    assert expected_model_ingestion_column in actual_table_columns


def test_load_functional_flow_from_excel_file_filter_non_mandatory_attributes():
    # Given
    pathname = os.path.dirname(__file__)
    filename = '2021-02-17-STDF_FUNCTIONAL_FLOW_RDM_V1.xlsx'
    excel_path = os.path.join(pathname, filename)
    expected_mandatory_attributes_count = 117

    # When
    actual_table = load_functional_flow_from_excel_file(excel_path)

    # Then
    assert expected_mandatory_attributes_count == len(actual_table)


def test_load_functional_flow_from_excel_file_returns_correct_columns():
    # Given
    pathname = os.path.dirname(__file__)
    filename = '2021-02-17-STDF_FUNCTIONAL_FLOW_RDM_V1.xlsx'
    excel_path = os.path.join(pathname, filename)
    expected_header = [
        'Mandatory', 'UC', 'Record_Name', 'Column_Name', 'Template', 'Example', 'Type',
        'Range', 'Description', 'Class', 'Sub_Class', 'Sub_Sub_Class', 'Class_Instance', 'Attribute'
    ]

    # When
    actual_header = load_functional_flow_from_excel_file(excel_path).columns

    diff = set(expected_header) ^ set(actual_header)

    # Then
    assert not diff
    assert len(set(expected_header)) == len(expected_header)


def test_load_functional_flow_from_excel_file_returns_table_with_class_instance_column_fdc():
    # Given
    expected_model_ingestion_column = 'Class_Instance'
    pathname = os.path.dirname(__file__)
    filename = '2021-02-17-FDC_FUNCTIONAL_FLOW_RDM_V1.xlsb.xlsx'
    excel_path = os.path.join(pathname, filename)

    # When
    actual_table_columns = load_functional_flow_from_excel_file(excel_path).columns

    # Then
    assert expected_model_ingestion_column in actual_table_columns


def test_load_functional_flow_from_excel_file_filter_non_mandatory_attributes_fdc():
    # Given
    pathname = os.path.dirname(__file__)
    filename = '2021-02-17-FDC_FUNCTIONAL_FLOW_RDM_V1.xlsb.xlsx'
    excel_path = os.path.join(pathname, filename)
    expected_mandatory_attributes_count = 38

    # When
    actual_table = load_functional_flow_from_excel_file(excel_path)

    # Then
    assert expected_mandatory_attributes_count == len(actual_table)


def test_load_functional_flow_from_excel_file_returns_table_with_class_instance_column_def():
    # Given
    expected_model_ingestion_column = 'Class_Instance'
    pathname = os.path.dirname(__file__)

    filename = '2021-02-17-DEF_FUNCTIONAL_FLOW_RDM_V1.xlsb.xlsx'
    excel_path = os.path.join(pathname, filename)

    # When
    actual_table_columns = load_functional_flow_from_excel_file(excel_path).columns

    # Then
    assert expected_model_ingestion_column in actual_table_columns


def test_load_functional_flow_from_excel_file_filter_non_mandatory_attributes_def():
    # Given
    pathname = os.path.dirname(__file__)
    filename = '2021-02-17-DEF_FUNCTIONAL_FLOW_RDM_V1.xlsb.xlsx'
    excel_path = os.path.join(pathname, filename)
    expected_mandatory_attributes_count = 31

    # When
    actual_table = load_functional_flow_from_excel_file(excel_path)

    # Then
    assert expected_mandatory_attributes_count == len(actual_table)
