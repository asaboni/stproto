from proto.domain.model.generate_normalized_owl import extract_class_names_from_excel, \
    extract_sub_class_names_from_excel, build_classes_from_class_names, get_relative_metaclass_name_from_class_name, \
    get_or_create_class_and_subclass_from_subclass_name, get_instance_class_of_attribute_name, \
    get_description_of_attribute_name, \
    create_classes_from_excel, create_attributes_and_annotations_from_excel, extract_sub_sub_class_names_from_excel, \
    get_or_create_class_subclass_and_sub_sub_class_from_subsubclass_name


from proto.infrastructure.model.load_model import load_sanitized_excel_file, generate_new_world_file, \
    load_functional_flow_from_excel_file

from owlready2 import Thing, World, types

import pytest
import os

STDF_TEST_IRI = 'http://st.com/test_stdf.owl'
FDC_TEST_IRI = 'http://st.com/test_fdc.owl'


@pytest.fixture()
def stdf_excel_file_new_template():
    """Fixture to load sample excel file for stdf OWL generation"""

    pathname = os.path.dirname(__file__)
    excel_filename = '2021-02-17-STDF_FUNCTIONAL_FLOW_RDM_V1.xlsx'
    excel_file = load_functional_flow_from_excel_file(os.path.join(pathname, excel_filename))
    return excel_file


@pytest.fixture()
def fdc_excel_file_new_template():
    """Fixture to load sample excel file for fdc OWL generation"""

    pathname = os.path.dirname(__file__)
    excel_filename = '2021-02-17-FDC_FUNCTIONAL_FLOW_RDM_V1.xlsb.xlsx'
    excel_file = load_functional_flow_from_excel_file(os.path.join(pathname, excel_filename))
    return excel_file


@pytest.fixture()
def def_excel_file_new_template():
    """Fixture to load sample excel file for defectivity OWL generation"""

    pathname = os.path.dirname(__file__)
    excel_filename = '2021-02-17-DEF_FUNCTIONAL_FLOW_RDM_V1.xlsb.xlsx'
    excel_file = load_functional_flow_from_excel_file(os.path.join(pathname, excel_filename))
    return excel_file


@pytest.fixture()
def reset_test_world():
    """Fixture to reset test OWL world"""

    pathname = os.path.dirname(__file__)
    world_path = 'test_owl.sqlite3'
    world_file = os.path.join(pathname, world_path)
    test_world = World()
    generate_new_world_file(world_file)
    test_world.set_backend(backend='sqlite', filename=os.path.join(pathname, world_path))
    return test_world


def test_create_classes_from_excel_creates_correct_classes_and_subclasses(reset_test_world, stdf_excel_file_new_template):

    # Given
    onto = reset_test_world.get_ontology(STDF_TEST_IRI)
    expected_class_names = ['EVENT', 'MEASUREMENT', 'INSTRUCTION', 'TEST_PROGRAM', 'PARAMETER', 'AGREGATION',
                            'MATERIAL',
                            'LOT', 'DIE', 'UNIT', 'EQUIPMENT', 'TESTER', 'UWA', 'TEST_STEP', 'WAFER', 'PTR', 'RESULT',
                            'INDICATOR', 'COMPUTATION', 'FLOW_ID', 'SETUP_PROBER', 'RESSOURCE', 'PROBE_CARD', 'FTR',
                            'MPR']

    # When
    with onto:
        create_classes_from_excel(stdf_excel_file_new_template, onto)

        actual_class_names = list(onto.classes())
        print(list(onto.classes()))

    # Then
    assert len(actual_class_names) == len(expected_class_names)



def test_create_classes_from_excel_creates_correct_classes_and_subclasses_for_fdc(reset_test_world,
                                                                                  fdc_excel_file_new_template):
    # Given
    onto = reset_test_world.get_ontology(FDC_TEST_IRI)
    expected_class_names = ['EVENT', 'MEASUREMENT', 'INSTRUCTION', 'PARAMETER', 'MATERIAL', 'LOT',
                            'PROCESS_CONTROL_DECISION', 'UWA', 'EQUIPMENT',
                            'SPC_RULES', 'WAFER', 'PROCESS_CONTROL', 'OPERATION', 'RECIPE', "MACHINE", "MODULE",
                            "ROUTE"]
    # When
    with onto:
        create_classes_from_excel(fdc_excel_file_new_template, onto)
        actual_class_names = list(onto.classes())
        print(list(onto.classes()))

    # Then
    assert len(actual_class_names) == len(expected_class_names)


def test_create_classes_from_excel_creates_correct_classes_and_subclasses_for_def(reset_test_world,
                                                                                  def_excel_file_new_template):
    # Given
    onto = reset_test_world.get_ontology(FDC_TEST_IRI)
    expected_class_names = ['EVENT', 'MEASUREMENT', 'INSTRUCTION', 'PARAMETER', 'MATERIAL', 'LOT',
                            'UWA', 'EQUIPMENT', 'WAFER', 'RECIPE', 'INSPECTION_TOOL', 'LAYER', 'DEFECTIVITY',
                            'AGREGATION_RESULT']
    # When
    with onto:
        create_classes_from_excel(def_excel_file_new_template, onto)
        actual_class_names = list(onto.classes())
        print(list(onto.classes()))

    # Then
    assert len(actual_class_names) == len(expected_class_names)


def test_create_attributes_and_annotations_from_excel_creates_correct_attributes(reset_test_world,
                                                                                 stdf_excel_file_new_template):
    # Given
    onto = reset_test_world.get_ontology(STDF_TEST_IRI)
    expected_attributes_count = 117

    # When
    with onto:
        create_classes_from_excel(stdf_excel_file_new_template, onto)
        actual_attributes_count = len(create_attributes_and_annotations_from_excel(stdf_excel_file_new_template, onto))

    # Then
    assert actual_attributes_count == expected_attributes_count


def test_extract_class_names_from_excel_returns_first_level_class_names(stdf_excel_file_new_template):
    # Given
    expected_classes = ['EVENT', 'EQUIPMENT', 'INSTRUCTION', 'EVENT',
                        'MATERIAL', 'UWA', 'PARAMETER', 'RESSOURCE']

    # When
    actual_classes = extract_class_names_from_excel(stdf_excel_file_new_template)
    diff = set(expected_classes) ^ set(actual_classes)

    # Then
    assert not diff
    assert len(actual_classes) == len(set(actual_classes))


def test_extract_sub_classes_from_excel_returns_second_level_class_names(stdf_excel_file_new_template):
    # Given
    expected_subclasses = ['MEASUREMENT', 'TEST_PROGRAM', 'AGREGATION', 'LOT', 'DIE', 'UNIT', 'TESTER',
                           'TEST_STEP', 'WAFER', 'PTR', 'RESULT', 'COMPUTATION', 'INDICATOR',
                           'FTR', 'MPR', 'PROBE_CARD', 'SETUP_PROBER']

    # When
    actual_subclasses = extract_sub_class_names_from_excel(stdf_excel_file_new_template)
    diff = set(expected_subclasses) ^ set(actual_subclasses)

    # Then
    assert not diff
    assert len(actual_subclasses) == len(set(actual_subclasses))


def test_extract_sub_sub_classes_from_excel_returns_third_level_class_names(stdf_excel_file_new_template):
    # Given
    expected_sub_sub_class_names = ['FLOW_ID', 'PTR', 'FTR', 'MPR']
    # When
    actual_sub_sub_classes = extract_sub_sub_class_names_from_excel(stdf_excel_file_new_template)
    diff = set(expected_sub_sub_class_names) ^ set(actual_sub_sub_classes)

    # Then
    assert not diff
    assert len(actual_sub_sub_classes) == len(set(actual_sub_sub_classes))



def test_build_classes_from_class_names_returns_full_constructed_classes(stdf_excel_file_new_template, reset_test_world):

    # Given
    onto = reset_test_world.get_ontology(STDF_TEST_IRI)
    class_names = stdf_excel_file_new_template['Class'].unique()
    expected_classes = ['EVENT', 'EQUIPMENT', 'INSTRUCTION', 'EVENT',
                        'MATERIAL', 'UWA', 'PARAMETER', 'RESSOURCE']

    # When
    with onto:
        actual_classes = build_classes_from_class_names(class_names, onto, stdf_excel_file_new_template)['class_name']
    diff = set(expected_classes) ^ set(actual_classes)

    # Then
    assert not diff
    assert len(actual_classes) == len(set(actual_classes))


def test_build_classes_from_sub_sub_class_names_returns_full_constructed_classes(stdf_excel_file_new_template,
                                                                                 reset_test_world):
    # Given
    onto = reset_test_world.get_ontology(STDF_TEST_IRI)
    sub_sub_class_names = extract_sub_sub_class_names_from_excel(stdf_excel_file_new_template)
    expected_class_names = ['FLOW_ID', 'PTR', 'FTR', 'MPR']

    # When
    with onto:
        actual_class_names = build_classes_from_class_names(sub_sub_class_names, onto, stdf_excel_file_new_template)[
            'class_name']
    diff = set(expected_class_names) ^ set(actual_class_names)

    # Then
    assert not diff
    assert len(actual_class_names) == len(set(actual_class_names))


def test_get_relative_metaclass_name_from_class_name_returns_correct_metaclass_name_from_class_name(stdf_excel_file_new_template):

    # Given
    expected_metaclass, expected_metaclass_name = Thing, 'Thing'
    test_class_name = 'PROCESS_STATE'

    # When
    actual_metaclass_name = get_relative_metaclass_name_from_class_name(test_class_name, stdf_excel_file_new_template)

    # Then
    assert actual_metaclass_name == expected_metaclass_name



def test_get_relative_metaclass_name_from_class_name_returns_correct_metaclass_name_from_sub_class_name(stdf_excel_file_new_template):


    # Given
    expected_sub_sub_class_name = 'FLOW_ID'
    expected_sub_class_name = 'TEST_STEP'
    expected_metaclass_name = 'UWA'

    # When
    actual_subclass_name = get_relative_metaclass_name_from_class_name(expected_sub_sub_class_name,
                                                                       stdf_excel_file_new_template)
    actual_metaclass_name = get_relative_metaclass_name_from_class_name(actual_subclass_name,
                                                                        stdf_excel_file_new_template)

    # Then
    assert actual_subclass_name == expected_sub_class_name
    assert actual_metaclass_name == expected_metaclass_name


def test_create_class_and_subclass_from_subclass_name_returns_correct_classes_from_sub_sub_class_names(
        reset_test_world):
    # Given
    onto = reset_test_world.get_ontology(STDF_TEST_IRI)
    expected_sub_sub_class_name = 'FLOW_ID'
    expected_sub_class_name = 'TEST_STEP'
    expected_metaclass_name = 'STEP'

    # When
    actual_sub_sub_class, actual_subclass, actual_metaclass = get_or_create_class_subclass_and_sub_sub_class_from_subsubclass_name(
        onto,
        expected_metaclass_name,
        expected_sub_class_name,
        expected_sub_sub_class_name)

    diff = set([expected_sub_sub_class_name, expected_sub_class_name, expected_metaclass_name]) ^ set(
        [actual_sub_sub_class._name, actual_subclass._name, actual_metaclass._name])

    # Then
    assert not diff
    assert actual_subclass in actual_sub_sub_class.ancestors()
    assert len(actual_sub_sub_class.is_a) == 1


def test_create_class_and_subclass_from_subclass_name_returns_correct_classes_from_names(reset_test_world):
    # Given
    onto = reset_test_world.get_ontology(STDF_TEST_IRI)
    expected_sub_class_name = 'MEASUREMENT'
    expected_metaclass_name = 'EVENT'

    # When
    actual_sub_class, actual_metaclass = get_or_create_class_and_subclass_from_subclass_name(expected_metaclass_name,
                                                                                             onto,
                                                                                             expected_sub_class_name)

    diff = set([expected_sub_class_name, expected_metaclass_name]) ^ set(
        [actual_sub_class._name, actual_metaclass._name])

    # Then
    assert not diff
    assert actual_metaclass in actual_sub_class.ancestors()
    assert len(actual_sub_class.is_a) == 1


def test_create_class_and_subclass_from_subclass_name_creates_three_levels_of_classes(reset_test_world):
    # Given
    onto = reset_test_world.get_ontology(STDF_TEST_IRI)
    expected_sub_sub_class_name = 'FLOW_ID'
    expected_sub_class_name = 'TEST_STEP'
    expected_metaclass_name = 'STEP'

    # When
    get_or_create_class_and_subclass_from_subclass_name(expected_metaclass_name, onto, expected_sub_class_name)
    get_or_create_class_and_subclass_from_subclass_name(expected_sub_class_name, onto, expected_sub_sub_class_name)

    # Then
    assert len(list(onto.classes())) == 3


def test_create_class_and_subclass_from_subclass_name_creates_only_once_classes(reset_test_world):
    # Given
    onto = reset_test_world.get_ontology(STDF_TEST_IRI)
    expected_sub_class_name = 'EVENT'
    expected_metaclass_name = 'MEASUREMENT'

    # When
    get_or_create_class_and_subclass_from_subclass_name(expected_metaclass_name, onto, expected_sub_class_name)
    get_or_create_class_and_subclass_from_subclass_name(expected_metaclass_name, onto, expected_sub_class_name)

    # Then
    assert len(list(onto.classes())) == 2


def test_get_instance_class_of_attribute_name_returns_correct_class_for_third_level_class_name(reset_test_world,
                                                                                               stdf_excel_file_new_template):
    # Given
    onto = reset_test_world.get_ontology(STDF_TEST_IRI)
    class_name = 'TEST_STEP'
    attribute_name = 'stdf_test_proc_id'

    with onto:
        expected_class = types.new_class(class_name, (Thing,))

    # When
    actual_class = get_instance_class_of_attribute_name(attribute_name, onto, stdf_excel_file_new_template)

    # Then
    assert actual_class == expected_class


def test_get_class_of_attribute_returns_correct_instance_class_of_attribute_name_listed_in_multiples_classes(
        reset_test_world, stdf_excel_file_new_template):

    # Given
    onto = reset_test_world.get_ontology(STDF_TEST_IRI)
    metaclass_name = "PRODUCT"
    subclass_name = 'WAFER'

    attribute_name = 'stdf_wafer_size'

    with onto:
        get_or_create_class_and_subclass_from_subclass_name(metaclass_name, onto, subclass_name)
        expected_class = onto[subclass_name]

    # When
    actual_class = get_instance_class_of_attribute_name(attribute_name, onto, stdf_excel_file_new_template)

    # Then
    assert actual_class == expected_class


# Add test for null class instance value
def test_get_class_of_attribute_returns_correct_instance_class_of_unique_attribute_name(reset_test_world,
                                                                                        stdf_excel_file_new_template):
    # Given
    onto = reset_test_world.get_ontology(STDF_TEST_IRI)
    metaclass_name = "PRODUCT"
    subclass_name = 'WAFER'

    attribute_name = 'stdf_wafer_size'

    with onto:
        get_or_create_class_and_subclass_from_subclass_name(metaclass_name, onto, subclass_name)
        expected_class = onto[subclass_name]

    # When
    actual_class = get_instance_class_of_attribute_name(attribute_name, onto, stdf_excel_file_new_template)

    # Then
    assert actual_class == expected_class


def test_get_none_as_description_if_attribute_is_not_mandatory(stdf_excel_file_new_template):
    # Given
    non_mandatory_attribute_name = 'stdf_setup_time'
    expected_description = None
    actual_description = get_description_of_attribute_name(non_mandatory_attribute_name, stdf_excel_file_new_template)

    # Then
    assert actual_description is expected_description


def test_get_description_of_attribute_name_returns_correct_description_new_version(stdf_excel_file_new_template):
    # Given
    attribute_name = 'stdf_test_proc_id'
    expected_description = 'Fabrication process ID'
    actual_description = get_description_of_attribute_name(attribute_name, stdf_excel_file_new_template)

    # Then
    assert actual_description == expected_description
