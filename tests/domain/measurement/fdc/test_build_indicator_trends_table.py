import datetime

import pytest
from pandas.testing import assert_series_equal

import pandas as pd
import numpy as np

from proto.infrastructure.measurement.fdc.load_fdc_tables import load_table_from_dump_name

from proto.domain.measurement.fdc.indicators_and_temporal_traces import build_indicators_trend_table, build_PCB_limits_from_FDC_limits, \
    generate_alarm_with_label_from_limits_value_series_and_alarm_level, build_mes_context_history_table, \
    extract_ms_from__event_time_column, merge_context_history_tables, \
    build_normalized_mes_context_table_from_mes_context, build_contextualized_treated_history, \
    build_temporal_traces_table, build_first_temporal_table


@pytest.fixture()
def pcb_alarms_and_levels():
    alarms_and_levels = [{
        'label': 'LSL',
        'alarm_level': -3
    }, {
        'label': 'LML',
        'alarm_level': -2
    }, {
        'label': 'LCL',
        'alarm_level': -1
    }, {
        'label': 'UCL',
        'alarm_level': 1
    }, {
        'label': 'UML',
        'alarm_level': 2
    }, {
        'label': 'USL',
        'alarm_level': 3
    }]

    return alarms_and_levels


@pytest.fixture()
def normalized_mes_context_table(mes_context_definition_table, mes_association_table):
    normalized_mes_context_table = build_normalized_mes_context_table_from_mes_context(mes_context_definition_table,
                                                                                       mes_association_table)
    return normalized_mes_context_table


@pytest.fixture()
def indicators_table():
    name = 'PCB.Indicators.dump'
    indicator_table_header = ['indicator_id', 'strategy_id', 'label',
                              'ltrs', 'ltri', 'filter_id', 'initial_target']
    indicator_table_types = {
        'indicator_id': np.int64,
        'strategy_id': str,
        'label': str,
        'ltrs': str,
        'ltri': str,
        'filter_id': np.int64,
        'initial_target': np.float64
    }
    indicators_METADATA = {
        'filename': name,
        'names': indicator_table_header,
        'types': indicator_table_types
    }
    table = load_table_from_dump_name(indicators_METADATA)
    return table


@pytest.fixture()
def strategies_table():
    name = 'PCB.Strategies.dump'
    strategies_table_header = ['strategy_id', 'label', 'dcqualitylevel', 'moduleid',
                               'dceventstemplateid', 'dceventsconfigid']

    strategies_table_types = {
        'strategy_id': str,
        'label': str,
        'dcqualitylevel': np.float64,
        'moduleid': str,
        'dceventstemplateid': str,
        'dceventsconfigid': np.int64
    }

    strategies_METADATA = {
        'filename': name,
        'names': strategies_table_header,
        'types': strategies_table_types
    }
    table = load_table_from_dump_name(strategies_METADATA)
    return table


@pytest.fixture()
def treated_data_history_table():
    name = 'PCB.TreatedDataHistory.dump'
    treated_data_history_table_header = ['context_id', 'indicator_id', 'value']

    treated_data_history_table_types = {
        'context_id': np.int64,
        'indicator_id': np.int64,
        'value': np.float64
    }

    treated_data_history_METADATA = {
        'filename': name,
        'names': treated_data_history_table_header,
        'types': treated_data_history_table_types
    }
    table = load_table_from_dump_name(treated_data_history_METADATA)
    return table


@pytest.fixture()
def mes_context_definition_table():
    name = 'PCB.MESContextHistory.dump'
    mes_context_history_table_header = ['mes_context_id', 'technology_name', 'product_name', 'lot_name', 'wafer_name',
                                        'route_name', 'stage_name', 'operation_name', 'aux1', 'aux2']
    mes_context_history_table_types = {
        'mes_context_id': np.float64,
        'technology_name': str,
        'product_name': str,
        'lot_name': str,
        'wafer_name': str,
        'route_name': str,
        'stage_name': str,
        'operation_name': str,
        'aux1': str,
        'aux2': str
    }

    mes_context_definition_METADATA = {
        'filename': name,
        'names': mes_context_history_table_header,
        'types': mes_context_history_table_types,
        'null_value': 'N.A.'
    }

    table = load_table_from_dump_name(mes_context_definition_METADATA)
    return table


@pytest.fixture()
def mes_association_table():
    name = 'PCB.MESFDCAssociation.dump'
    mes_association_table_header = ['context_id', 'mes_context_id']
    mes_association_table_types = {
        'context_id': np.int64,
        'mes_context_id': np.int64,
    }

    mes_association_METADATA = {
        'filename': name,
        'names': mes_association_table_header,
        'types': mes_association_table_types,
        'null_value': 'N.A.'
    }
    table = load_table_from_dump_name(mes_association_METADATA)
    return table


@pytest.fixture()
def context_history_table():
    name = 'PCB.ContextHistory.dump'
    context_history_table_header = ['context_id', 'material_id', 'recipe_id', 'start_recipe_step',
                                    'equipment_id', 'module_id', 'Strategy_name', 'Strategy_id',
                                    'steady_event_time', 'start_event_time', 'stop_event_time',
                                    'dc_quality_value', 'completed', 'is_reference', 'is_archived',
                                    'archive_file', 'history_id']

    context_history_table_types = {
        'context_id': np.float64,
        'material_id': str,
        'recipe_id': str,
        'start_recipe_step': np.int64,
        'equipment_id': str,
        'module_id': str,
        'Strategy_name': str,
        'Strategy_id': str,
        'steady_event_time': str,
        'start_event_time': str,
        'stop_event_time': str,
        'dc_quality_value': np.float64,
        'completed': str,
        'is_reference': str,
        'is_archived': str,
        'archive_file': str,
        'history_id': np.int64
    }
    context_history_METADATA = {
        'filename': name,
        'names': context_history_table_header,
        'types': context_history_table_types
    }

    table = load_table_from_dump_name(context_history_METADATA)
    return table


@pytest.fixture()
def limits_table():
    name = 'PCB.Limits.dump'
    limits_table_header = ['limit_id', 'indicator_id', 'label', 'value', 'alarm_level']
    limits_table_types = {
        'limit_id': np.int64,
        'indicator_id': np.int64,
        'label': str,
        'value': np.float64,
        'alarm_level': np.int64
    }
    limits_METADATA = {
        'filename': name,
        'names': limits_table_header,
        'types': limits_table_types
    }
    table = load_table_from_dump_name(limits_METADATA)
    return table


@pytest.fixture()
def collected_data_history_table():
    name = 'PCB.CollectedDataHistory_0.dump'
    collected_data_history_table_header = ['context_id', 'variable_id', 'time_stamp', 'time_stamp_ms', 'value']
    collected_data_history_table_types = {
        'context_id': np.float64,
        'variable_id': np.int64,
        'time_stamp': str,
        'time_stamp_ms': str,
        'value': np.float64
    }
    collected_data_METADATA = {
        'filename': name,
        'names': collected_data_history_table_header,
        'types': collected_data_history_table_types
    }
    table = load_table_from_dump_name(collected_data_METADATA)
    return table


@pytest.fixture()
def variables_table():
    name = 'PCB.Variables.dump'
    variables_table_header = ['variable_id', 'label', 'unit', 'min_value', 'max_value', 'alias']
    variables_table_types = {
        'variable_id': np.int64,
        'label': str,
        'unit': str,
        'min_value': np.float64,
        'max_value': np.float64,
        'alias': str
    }

    variables_METADATA = {
        'filename': name,
        'names': variables_table_header,
        'types': variables_table_types
    }
    table = load_table_from_dump_name(variables_METADATA)
    return table


def test_build_temporal_trends_table(context_history_table,
                                     normalized_mes_context_table,
                                     collected_data_history_table,
                                     variables_table):
    # Given
    expected_temporal_table_columns = ['starteventtime', 'duration_ms', 'context_id', 'fab', 'ingestion_date',
                                       'equipment', 'module', 'material', 'recipe', 'recipe_start_step',
                                       'strategy', 'dcqv', 'technology', 'cam_product',
                                       'lot_id', 'wafer_id', 'stage', 'operation', 'aux1', 'aux2',
                                       'variable', 'variable_alias', 'value', 'yearmonth']

    # When
    normalized_mes_history_context_table = merge_context_history_tables(context_history_table,
                                                                        normalized_mes_context_table)
    actual_temporal_table = build_temporal_traces_table(normalized_mes_history_context_table,
                                                        collected_data_history_table,
                                                        context_history_table,
                                                        variables_table)
    diff = set(actual_temporal_table.columns) ^ set(expected_temporal_table_columns)

    # Then
    assert not diff
    assert len(set(actual_temporal_table.columns)) == len(expected_temporal_table_columns)


def test_build_indicators_trends_table(context_history_table,
                                       mes_context_definition_table,
                                       mes_association_table,
                                       treated_data_history_table,
                                       indicators_table,
                                       strategies_table,
                                       limits_table):
    # Given
    expected_indicator_table_columns = ['context_id', 'module_id', 'material', 'recipe', 'starteventtime',
                                        'starteventtime_ms', 'strategy', 'technology', 'cam_product', 'lot_id',
                                        'wafer_id', 'stage', 'operation', 'aux1', 'aux2', 'indicator', 'value',
                                        'LSL', 'LML', 'LCL', 'UCL', 'UML', 'USL', 'recipe_start_step', 'dcqv', 'dcql']

    # When
    actual_indicators_table = build_indicators_trend_table(context_history_table,
                                                           mes_context_definition_table,
                                                           mes_association_table,
                                                           treated_data_history_table,
                                                           indicators_table,
                                                           strategies_table,
                                                           limits_table)
    diff = set(actual_indicators_table.columns) ^ set(expected_indicator_table_columns)

    # Then
    assert not diff
    assert len(set(actual_indicators_table.columns)) == len(actual_indicators_table.columns)


def test_build_first_temporal_table(collected_data_history_table, context_history_table, variables_table):
    # Given
    expected_first_temporal_table_columns = ['context_id', 'module', 'material', 'recipe', 'starteventtime',
                                             'strategy', 'variable', 'value', 'duration_ms', 'recipe_start_step',
                                             'dcqv', 'variable_alias']
    # When
    actual_first_temporal_table_columns = build_first_temporal_table(collected_data_history_table,
                                                                     context_history_table,
                                                                     variables_table)
    diff = set(actual_first_temporal_table_columns.columns) ^ set(expected_first_temporal_table_columns)

    # Then
    assert not diff
    assert len(set(actual_first_temporal_table_columns.columns)) == len(actual_first_temporal_table_columns.columns)


def test_build_contextualized_treated_history_returns_correct_structure(context_history_table,
                                                                        normalized_mes_context_table,
                                                                        treated_data_history_table,
                                                                        indicators_table,
                                                                        strategies_table):
    # Given
    expected_indicator_first_table_columns = ['indicator_id', 'context_id', 'module_id', 'material', 'recipe',
                                              'starteventtime', 'starteventtime_ms', 'strategy', 'indicator', 'value',
                                              'recipe_start_step', 'dcqv', 'dcql']

    # When
    normalized_mes_history_context_table = merge_context_history_tables(context_history_table,
                                                                        normalized_mes_context_table)
    actual_indicators_first_table = build_contextualized_treated_history(normalized_mes_history_context_table,
                                                                         treated_data_history_table,
                                                                         indicators_table,
                                                                         strategies_table)

    diff = set(actual_indicators_first_table.columns) ^ set(expected_indicator_first_table_columns)

    # Then
    assert not diff
    assert len(set(actual_indicators_first_table.columns)) == len(expected_indicator_first_table_columns)


def test_merge_context_history_tables_returns_correct_struct_and_data(normalized_mes_context_table,
                                                                      context_history_table):
    # Given
    expected_merge_columns = ['context_id', 'material_id', 'recipe_id', 'start_recipe_step', 'equipment_id',
                              'module_id', 'Strategy_name', 'Strategy_id', 'steady_event_time', 'start_event_time',
                              'stop_event_time', 'dc_quality_value', 'completed', 'is_reference', 'is_archived',
                              'archive_file', 'history_id', 'loading_day', 'mes_context_id', 'technology',
                              'cam_product', 'lot_id', 'wafer_id', 'stage', 'operation', 'aux1', 'aux2', 'equipment',
                              'yearmonth', 'fab', 'starteventtime_ms', 'stopeventtime_ms']

    # When
    actual_merged_columns = merge_context_history_tables(context_history_table, normalized_mes_context_table)
    diff = set(actual_merged_columns) ^ set(expected_merge_columns)

    # Then
    assert not diff
    assert len(set(actual_merged_columns)) == len(expected_merge_columns)


def test_build_normalized_mes_context_table_from_normalised_mes_context_associates_correct_contexts(
        mes_context_definition_table,
        mes_association_table):
    # Given
    expected_contexts = [28961073, 28961074, 28961075, 28961078, 28961079, 28961080, 28961081, 28961085, 28961086,
                         28961087, 28961088]
    yesterday = datetime.date.today() - datetime.timedelta(days=1)
    # TODO: Declare correct nan values for string types, test values with nan
    expected_associated_table = pd.DataFrame({
        'context_id': expected_contexts,
        'loading_day': [yesterday] * len(expected_contexts),
        'mes_context_id': [54706] * len(expected_contexts),
        'technology': ['nan'] * len(expected_contexts),
        'cam_product': ['FF276CFG-07'] * len(expected_contexts),
        'lot_id': ['G037853'] * len(expected_contexts),
        'wafer_id': ['nan'] * len(expected_contexts),
        'stage': ['nan'] * len(expected_contexts),
        'operation': ['7160'] * len(expected_contexts),
        'aux1': ['nan'] * len(expected_contexts),
        'aux2': [float('NaN')] * len(expected_contexts),
        'equipment': ['MSP07'] * len(expected_contexts),
        'yearmonth': ['202011'] * len(expected_contexts)
    })

    # When
    actual_associated_table = build_normalized_mes_context_table_from_mes_context(mes_context_definition_table,
                                                                                  mes_association_table)
    diff = set(actual_associated_table.columns) ^ set(expected_associated_table)
    # Then
    assert not diff
    assert len(set(actual_associated_table.columns)) == len(expected_associated_table.columns)
    assert len(actual_associated_table) == len(expected_contexts)


def test_build_normalized_mes_context_table_from_normalised_mes_context_associates_correct_columns(
        mes_context_definition_table,
        mes_association_table):
    # Given
    expected_columns = ['context_id', 'loading_day', 'mes_context_id', 'technology', 'cam_product', 'lot_id',
                        'wafer_id', 'stage', 'operation', 'aux1', 'aux2', 'equipment', 'yearmonth']

    # When
    actual_associated_table = build_normalized_mes_context_table_from_mes_context(mes_context_definition_table,
                                                                                  mes_association_table)
    diff = set(actual_associated_table.columns) ^ set(expected_columns)
    # Then
    assert not diff
    assert len(set(actual_associated_table)) == len(expected_columns)


def test_build_mes_context_history_table_retrieves_correct_history_with_yesterdays_date(mes_context_definition_table):
    # Given
    expected_mes_context_history_table_columns = ['loading_day', 'mes_context_id', 'technology_name', 'product_name',
                                                  'lot_name', 'wafer_name', 'route_name', 'stage_name',
                                                  'operation_name', 'aux1', 'aux2']

    # When
    actual_indicators_table = build_mes_context_history_table(mes_context_definition_table)
    diff = set(actual_indicators_table.columns) ^ set(expected_mes_context_history_table_columns)

    # Then
    assert not diff
    assert len(set(actual_indicators_table.columns)) == len(actual_indicators_table.columns)


def test_extract_ms_from__event_time_column():
    # Given
    expected_ms_serie = pd.Series([510, 510, 510, 730, 760, 290, 290, 850, 850, 170, 680, 680, 170, 170, 180, 180])
    parsed_time_serie = pd.Series(['2020110820333151', '2020110820333151', '2020110820333151', '2020110820331773',
                                   '2020110820335976', '2020110820342829', '2020110820342829', '2020110820334985',
                                   '2020110820334985', '2020110820340217', '2020110820345968', '2020110820345968',
                                   '2020110820340517', '2020110820340517', '2020110820343918', '2020110820343918'])

    # When
    actual_ms_serie = extract_ms_from__event_time_column(parsed_time_serie)

    # Then
    assert_series_equal(expected_ms_serie, actual_ms_serie)


def test_build_PCB_limits_from_FDC_limits_returns_correct_columns(limits_table):
    # Given
    expected_pcb_limits_columns = ['loading_day', 'indicator_id', 'value', 'LSL', 'LML', 'LCL', 'UCL', 'UML', 'USL']

    # When
    actual_pcb_limits = build_PCB_limits_from_FDC_limits(limits_table)

    # Then
    diff = set(actual_pcb_limits.columns) ^ set(expected_pcb_limits_columns)
    assert not diff
    assert len(set(actual_pcb_limits.columns)) == len(actual_pcb_limits.columns)
    assert True


def test_build_PCB_limits_from_FDC_limits_returns_correct_index_for_values(limits_table):
    # Given
    expected_pcb_values_ordered_list = limits_table['value'].copy()

    # When
    actual_pcb_values_ordered_list = build_PCB_limits_from_FDC_limits(limits_table)['value'].copy()

    # Then
    assert_series_equal(expected_pcb_values_ordered_list, actual_pcb_values_ordered_list)


def test_generate_alarm_with_label_from_limits_value_series_and_alarm_level_generates_correct_alarm_for_alarm_level(
        limits_table, pcb_alarms_and_levels):
    # Given
    unit_limit_LSL_alarm = pd.DataFrame({
        'value': [5.8000000],
        'alarm_level': [-3]
    })
    expected_generated_labels = [unit_limit_LSL_alarm['value'].tolist()[0], None, None, None, None, None]
    # When
    actual_generated_labels = []
    for alarm in pcb_alarms_and_levels:
        actual_generated_labels.append(generate_alarm_with_label_from_limits_value_series_and_alarm_level(
            unit_limit_LSL_alarm,
            alarm['alarm_level']).tolist()[0])

    # Then
    diff = set(actual_generated_labels) ^ set(expected_generated_labels)
    assert not diff
    assert len(actual_generated_labels) == len(actual_generated_labels)
