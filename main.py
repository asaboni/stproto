from fastapi import FastAPI, File, UploadFile
from starlette.responses import FileResponse

from proto.usecase.ingest_excel_normalisation_file import ingest_and_transform_excel_file

from proto.usecase.measurement.ingest_parsed_rousset_fdc_data import construct_temporal_trace_refined_table_from_rousset_fdc_data

from proto.usecase.measurement.ingest_parsed_rousset_fdc_data import construct_temporal_trace_refined_table_from_rousset_fdc_data


app = FastAPI()


@app.get("/excel_to_owl")
async def excel_to_owl():
    """
     Transform attributes file into an owl data model.

     This will let the API user (some external developer) launch a script for this transformation.

     And this path operation will:

     * Load test stdf attributes file.
     * Parse each line and generate an attribute, and its associated classes & subclasses if not already declared..
     * Send a notification back to the API user, as a return.
         * At this point the return corresponds to the total classes & attributes generated
     """
    classes, attributes = ingest_and_transform_excel_file()

    state = '{classes} classes and {attributes} attributes have been parsed'.format(
        classes=classes,
        attributes=attributes
    )
    return {
        "message": str(state)

    }


@app.get("/fdc_rousset_temporal_traces")
async def fdc_rousset_temporal_traces():
    """
    Transform rousset data to extract temporal traces.

    This will let the API user (some external developer) launch a job for this extraction.

    And this path operation will:

    * Load test dump files (with no parameter yet).
    * Apply transformations on rawd ata.
    * Send a notification back to the API user, as a return.
        * At this point the return corresponds to the total temporal traces generated
    """
    temporal_traces = construct_temporal_trace_refined_table_from_rousset_fdc_data()
    state = 'Total {temporal_traces} temporal traces have been parsed'.format(
        temporal_traces=len(temporal_traces)
    )
    return {
        "message": str(state)

    }


@app.get("/hello")
async def hello():
    state = "Hello_world"
    return {
        "message": str(state)
    }
