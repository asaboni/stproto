def levenshtein(seq1, seq2):
    size_x = len(seq1) + 1
    size_y = len(seq2) + 1
    matrix = np.zeros ((size_x, size_y))
    for x in range(size_x):
        matrix [x, 0] = x
    for y in range(size_y):
        matrix [0, y] = y

    for x in range(1, size_x):
        for y in range(1, size_y):
            if seq1[x-1] == seq2[y-1]:
                matrix [x,y] = min(
                    matrix[x-1, y] + 1,
                    matrix[x-1, y-1],
                    matrix[x, y-1] + 1
                )
            else:
                matrix [x,y] = min(
                    matrix[x-1,y] + 1,
                    matrix[x-1,y-1] + 1,
                    matrix[x,y-1] + 1
                )
    return (matrix[size_x - 1, size_y - 1])

def is_sequence_number_same(seq1, seq2):
    nb1 = re.findall(r'\d{1,2}', seq1)[0]
    nb2 = re.findall(r'\d{1,2}', seq2)[0]
    if int(nb1)==int(nb2):
        return True
    else:
        return False

def remove_digits_and_parenthesis(string):
    string_pattern = r'[\(*0-9\)*]'
    mod_string = re.sub(string_pattern, '', string)
    return mod_string