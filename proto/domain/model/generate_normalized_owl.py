from owlready2 import *
import pandas as pd
from owlready2 import Thing, types

NULL_CHAR = '#'


def create_classes_from_excel(excel_model_file, ontology):
    sub_class_names = extract_sub_class_names_from_excel(excel_model_file)
    sub_sub_class_names = extract_sub_sub_class_names_from_excel(excel_model_file)
    class_names = extract_class_names_from_excel(excel_model_file)
    subclasses = build_classes_from_class_names(sub_class_names, ontology, excel_model_file)
    subsubclasses = build_classes_from_class_names(sub_sub_class_names, ontology, excel_model_file)
    classes = build_classes_from_class_names(class_names, ontology, excel_model_file)
    return classes.append([subclasses, subsubclasses])


def extract_class_names_from_excel(excel_model_file):
    class_names = excel_model_file['Class'].unique()
    return class_names[class_names != NULL_CHAR].tolist()


def extract_sub_class_names_from_excel(excel_model_file):
    all_sub_classes = excel_model_file['Sub_Class'].unique()
    return all_sub_classes[all_sub_classes != NULL_CHAR].tolist()


def extract_sub_sub_class_names_from_excel(excel_model_file):
    all_sub_classes = excel_model_file['Sub_Sub_Class'].unique()
    return all_sub_classes[all_sub_classes != NULL_CHAR].tolist()


def build_classes_from_class_names(class_names, ontology, excel_model_file):
    with ontology:
        owl_classes_list = pd.DataFrame()
        for class_name in class_names:
            onto_class, metaclass = build_onto_class_from_class_name(class_name, ontology, excel_model_file)
            owl_classes_list = owl_classes_list.append({
                'metaclass': metaclass,
                'class_name': class_name,
                'onto_class': onto_class
            }, ignore_index=True)
    return owl_classes_list


def build_onto_class_from_class_name(class_name, ontology, excel_model_file):
    classes_with_no_subclasses = ['Measurement_Result']
    if class_name in excel_model_file['Sub_Class'].tolist():
        metaclass_name = get_relative_metaclass_name_from_class_name(class_name, excel_model_file)
        onto_class, onto_metaclass = get_or_create_class_and_subclass_from_subclass_name(metaclass_name, ontology,
                                                                                         class_name)
    elif class_name in excel_model_file['Sub_Sub_Class'].tolist():
        sub_class_name = get_relative_metaclass_name_from_class_name(class_name, excel_model_file)
        metaclass_name = get_relative_metaclass_name_from_class_name(sub_class_name, excel_model_file)
        onto_class, onto_sub_class, onto_metaclass = get_or_create_class_subclass_and_sub_sub_class_from_subsubclass_name(
            ontology,
            class_name,
            sub_class_name,
            metaclass_name)
    else:
        if class_name in classes_with_no_subclasses:
            pass
        onto_metaclass = Thing
        onto_class = types.new_class(class_name, (onto_metaclass,))
    return onto_class, onto_metaclass


def get_or_create_class_and_subclass_from_subclass_name(metaclass_name, onto, sub_class_name):
    with onto:
        if not onto[metaclass_name]:
            types.new_class(metaclass_name, (Thing,))

        sub_class = types.new_class(sub_class_name, (onto[metaclass_name],))

    return sub_class, onto[metaclass_name]


def get_or_create_class_subclass_and_sub_sub_class_from_subsubclass_name(onto,
                                                                         sub_sub_class_name,
                                                                         sub_class_name,
                                                                         metaclass_name):
    with onto:
        sub_class, metaclass = get_or_create_class_and_subclass_from_subclass_name(metaclass_name, onto, sub_class_name)

        sub_sub_class = types.new_class(sub_sub_class_name, (sub_class,))

    return sub_sub_class, sub_class, metaclass


def get_relative_metaclass_name_from_class_name(class_name, excel_model_file):
    metaclass_name = 'Thing'
    if class_name in excel_model_file['Sub_Class'].tolist():
        metaclass_name = excel_model_file['Class'][excel_model_file['Sub_Class'] == class_name].tolist()[0]
    elif class_name in excel_model_file['Sub_Sub_Class'].tolist():
        metaclass_name = excel_model_file['Sub_Class'][excel_model_file['Sub_Sub_Class'] == class_name].tolist()[0]
    return metaclass_name


def create_attributes_and_annotations_from_excel(excel_model_file, ontology):
    attributes = excel_model_file['Attribute']
    onto_attributes = []

    # Attach with annotation
    with ontology:
        for attribute in attributes:
            ontology_data_property = types.new_class(attribute, (DataProperty,))
            attached_onto_class = get_instance_class_of_attribute_name(attribute, ontology, excel_model_file)
            print(attribute)
            print(attached_onto_class)
            attribute_description = get_description_of_attribute_name(attribute, excel_model_file)
            ontology_data_property.domain = [attached_onto_class]
            ontology_data_property.comment = attribute_description
            onto_attributes.append(ontology_data_property)

    return onto_attributes


def get_instance_class_of_attribute_name(attribute, ontology, excel_model_file):
    if excel_model_file['Class_Instance'][excel_model_file['Attribute'] == attribute].tolist()[0] != NULL_CHAR:
        attached_class_name = excel_model_file['Class_Instance'][excel_model_file['Attribute'] == attribute].tolist()[0]
    else:
        attached_class_name = NULL_CHAR
    attached_onto_class = ontology[attached_class_name]
    return attached_onto_class


def get_description_of_attribute_name(attribute, excel_model_file):
    description_list = excel_model_file['Description'][excel_model_file['Attribute'] == attribute].tolist()
    if len(description_list) > 0:
        return description_list[0]
    return None
