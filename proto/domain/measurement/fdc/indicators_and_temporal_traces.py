import datetime
import pandas as pd

PCB_ALARMS_LABELS_AND_LEVELS = [{
    'label': 'LSL',
    'alarm_level': -3
}, {
    'label': 'LML',
    'alarm_level': -2
}, {
    'label': 'LCL',
    'alarm_level': -1
}, {
    'label': 'UCL',
    'alarm_level': 1
}, {
    'label': 'UML',
    'alarm_level': 2
}, {
    'label': 'USL',
    'alarm_level': 3
}]


# TODO : test with refined table
def build_temporal_traces_table(normalized_mes_history_context_table,
                                collected_data_history_table,
                                context_history_table,
                                variables_table):
    # Build intermediate view :                                                                                                                                                                                                                                                                                                                ch.start_recipe_step as recipe_start_step, ch.dc_quality_value as dcqv, v.alias as variable_alias
    intermediate_table = build_first_temporal_table(collected_data_history_table, context_history_table,
                                                    variables_table)
    merged_mes_context_history = merge_context_history_tables(context_history_table,
                                                              normalized_mes_history_context_table)
    temporale_traces = pd.merge(intermediate_table, merged_mes_context_history, how='left')
    temporale_traces = temporale_traces.assign(ingestion_date= lambda x: '2021-15')
    final_columns = ['starteventtime', 'duration_ms', 'context_id', 'fab', 'ingestion_date',
                     'equipment', 'module', 'material', 'recipe', 'recipe_start_step',
                     'starteventtime', 'strategy', 'dcqv', 'technology', 'cam_product',
                     'lot_id', 'wafer_id', 'stage', 'operation', 'aux1', 'aux2',
                     'variable', 'variable_alias', 'value', 'ingestion_date', 'yearmonth']
    return temporale_traces[final_columns]


# TODO : test with refined table
def build_indicators_trend_table(context_history_table,
                                 mes_context_definition_table,
                                 mes_association_table,
                                 treated_data_history_table,
                                 indicators_trends_table,
                                 strategies_table,
                                 limits_table):
    pcb_limits_table = build_PCB_limits_from_FDC_limits(limits_table)
    normalized_mes_context_table = build_normalized_mes_context_table_from_mes_context(mes_context_definition_table,
                                                                                       mes_association_table)
    merged_mes_context_history = merge_context_history_tables(context_history_table, normalized_mes_context_table)

    contextualized_treated_history = build_contextualized_treated_history(merged_mes_context_history,
                                                                          treated_data_history_table,
                                                                          indicators_trends_table,
                                                                          strategies_table)

    # Tested until here
    history_with_limits_table = pd.merge(contextualized_treated_history,
                                         pcb_limits_table,
                                         how='left')
    history_with_full_context = pd.merge(history_with_limits_table,
                                         normalized_mes_context_table,
                                         how='left')

    # To join :
    indicators_finale_columns = ['context_id', 'module_id', 'material', 'recipe', 'starteventtime', 'starteventtime_ms',
                                 'strategy', 'technology', 'cam_product', 'lot_id', 'wafer_id', 'stage',
                                 'operation', 'aux1', 'aux2', 'indicator', 'value', 'LSL', 'LML', 'UCL', 'UML', 'USL',
                                 'LCL', 'recipe_start_step', 'dcqv', 'dcql']
    indicators_trends_table = history_with_full_context[indicators_finale_columns]
    return indicators_trends_table


# Transformation : build temporal history with context, variables & collected data, with good column names
def build_first_temporal_table(collected_data_history_table, context_history_table, variables_table):
    intermediate_table = pd.merge(collected_data_history_table,
                                  context_history_table,
                                  how='left')
    second_table = pd.merge(intermediate_table,
                            variables_table,
                            how='left')
    first_temporal_table = second_table.rename(columns={
        'material_id': 'material',
        'module_id': 'module',
        'recipe_id': 'recipe',
        'start_event_time': 'starteventtime',
        'Strategy_name': 'strategy',
        'label': 'variable',
        'start_recipe_step': 'recipe_start_step',
        'dc_quality_value': 'dcqv',
        'alias': 'variable_alias'
    })

    first_temporal_table = first_temporal_table.assign(duration_ms=lambda x: 1)

    first_temporal_table['material'] = first_temporal_table['material'] \
        .apply(lambda x: sanitize_material_id_column(x)).copy()

    first_temporal_table_final_columns = ['context_id', 'module', 'material', 'recipe', 'starteventtime',
                                          'strategy', 'variable', 'value', 'duration_ms', 'recipe_start_step',
                                          'dcqv', 'variable_alias']
    return first_temporal_table[first_temporal_table_final_columns]


# Transformation : build history with context, indicators & strategies definition, and with good column names
def build_contextualized_treated_history(normalized_mes_context_table,
                                         treated_data_history_table,
                                         indicators_table,
                                         strategies_table):
    context_and_treated_data_table = pd.merge(normalized_mes_context_table,
                                              treated_data_history_table,
                                              how='left')
    indicators_and_strategies_table = pd.merge(indicators_table,
                                               strategies_table,
                                               how='left')
    contextualized_treated_history = pd.merge(context_and_treated_data_table,
                                              indicators_and_strategies_table,
                                              how='left')
    contextualized_treated_history = contextualized_treated_history.rename(columns={
        'material_id': 'material',
        'recipe_id': 'recipe',
        'start_event_time': 'starteventtime',
        'Strategy_name': 'strategy',
        'label': 'indicator',
        'start_recipe_step': 'recipe_start_step',
        'dc_quality_value': 'dcqv',
        'dcqualitylevel': 'dcql'
    })

    contextualized_treated_history['material'] = contextualized_treated_history['material'] \
        .apply(lambda x: sanitize_material_id_column(x)).copy()

    contextualized_treated_history_final_columns = ['indicator_id', 'context_id', 'module_id', 'material',
                                                    'recipe', 'starteventtime', 'starteventtime_ms', 'strategy',
                                                    'indicator', 'value', 'recipe_start_step', 'dcqv', 'dcql']
    return contextualized_treated_history[contextualized_treated_history_final_columns]


# Transformation, merge context with MES context definition, add env context & replace with good column names
# Filter on yearmonth, equipment, day => not in this example
def merge_context_history_tables(context_table, normalized_mes_context_history_table):
    yearmonth = '202011'
    equipment = 'MSPO7'
    fab = 'R8WF'
    merged_table = pd.merge(context_table, normalized_mes_context_history_table, how='left')
    context_history_table = merged_table \
        .assign(equipment=lambda x: equipment) \
        .assign(yearmonth=lambda x: yearmonth) \
        .assign(fab=lambda x: fab) \
        .assign(starteventtime_ms=lambda x: extract_ms_from__event_time_column(x['start_event_time'])) \
        .assign(stopeventtime_ms=lambda x: extract_ms_from__event_time_column(x['stop_event_time']))

    return context_history_table


# Transformation : normalise contexts between automation and fdc systems
def build_normalized_mes_context_table_from_mes_context(mes_context_table, mes_association_table):
    normalized_mes_context = pd.merge(mes_association_table,
                                      mes_context_table,
                                      how='left')
    yearmonth = '202011'
    equipment = 'MSPO7'
    yesterday = get_yesterday_formatted_date()
    associated_mes_context = normalized_mes_context \
        .assign(equipment=lambda x: equipment) \
        .assign(yearmonth=lambda x: yearmonth) \
        .assign(loading_day=lambda x: yesterday) \
        .rename(columns={
            'product_name': 'cam_product',
            'technology_name': 'technology',
            'lot_name': 'lot_id',
            'wafer_name': 'wafer_id',
            'stage_name': 'stage',
            'operation_name': 'operation'
        })
    final_columns = ['context_id', 'loading_day', 'mes_context_id', 'technology', 'cam_product', 'lot_id',
                     'wafer_id', 'stage', 'operation', 'aux1', 'aux2', 'equipment', 'yearmonth']
    return associated_mes_context[final_columns]


# Transformation : Distinct context ids, add loading date
def build_mes_context_history_table(mes_context_table):
    yesterday = get_yesterday_formatted_date()
    mes_context_table = mes_context_table.assign(loading_day=lambda x: yesterday).drop_duplicates()
    return mes_context_table


# Transformation : convert level to label & add refence informations
def build_PCB_limits_from_FDC_limits(limits_table):
    alarms = PCB_ALARMS_LABELS_AND_LEVELS
    yesterday = get_yesterday_formatted_date()
    pcb_limits = pd.DataFrame({}) \
        .assign(loading_day=lambda x: yesterday) \
        .assign(indicator_id=lambda x: limits_table['indicator_id']) \
        .assign(value=lambda x: limits_table['value'])
    for alarm in alarms:
        pcb_limits[alarm['label']] = generate_alarm_with_label_from_limits_value_series_and_alarm_level(
            limits_table,
            alarm['alarm_level'])
    return pcb_limits


# Transformation : generate label from level
def generate_alarm_with_label_from_limits_value_series_and_alarm_level(limits_table, alarm_level):
    to_return = limits_table.apply(lambda x: x['value'] if x['alarm_level'] == alarm_level else None, axis=1)
    return to_return


# Transformation : extract ms count from whole datetime
def extract_ms_from__event_time_column(event_time):
    event_ms_serie = pd.to_datetime(event_time, format='%Y%m%d%H%M%S%f') \
        .apply(lambda x: int(x.strftime('%f')) / 1000) \
        .astype(int)
    return event_ms_serie


# Transformation : get previous day as loading day, to change
def get_yesterday_formatted_date():
    return datetime.date.today() - datetime.timedelta(days=1)


# Transformation : remove space
def sanitize_material_id_column(material_id_serie):
    return material_id_serie.replace(' ', '')
