class Instruction:
    def __init__(self):
        self.id: str


class SPC_rules(Instruction):
    def __init__(self):
        self.id: str


class Post_processing(Instruction):
    def __init__(self):
        self.id: str


class Recipe(Instruction):
    def __init__(self):
        self.id: str


class Data_Collector(Instruction):
    def __init__(self):
        self.id: str
        self.triggering: str


class Fdc_collector(Data_Collector):
    def __init__(self):
        self.id: str
        self.sampling_strategy: str


class Report_collector(Data_Collector):
    def __init__(self):
        self.id: str
        self.sampling_rate: str
