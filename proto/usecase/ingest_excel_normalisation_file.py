import os

from owlready2 import get_ontology

from proto.domain.model.generate_normalized_owl import create_classes_from_excel, \
    create_attributes_and_annotations_from_excel
from proto.infrastructure.model.load_model import load_functional_flow_from_excel_file


def ingest_and_transform_excel_file():
    onto = get_ontology("http://st.com/stdf.owl")
    pathname = os.path.dirname(__file__)
    excel_filename = '2021-02-17-DEF_FUNCTIONAL_FLOW_RDM_V1.xlsb.xlsx'
    excel_file = load_functional_flow_from_excel_file(os.path.join(pathname, excel_filename))
    classes = create_classes_from_excel(excel_file, onto)
    attributes = create_attributes_and_annotations_from_excel(excel_file, onto)
    target_file = os.path.join(pathname, "defectivity_17_02_2021.owl")
    onto.save(file=target_file, format="rdfxml")
    return classes, attributes
