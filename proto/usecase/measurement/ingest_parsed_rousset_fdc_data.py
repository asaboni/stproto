import numpy as np
from proto.domain.measurement.fdc.indicators_and_temporal_traces import merge_context_history_tables, build_temporal_traces_table, \
    build_normalized_mes_context_table_from_mes_context, build_indicators_trend_table
from proto.infrastructure.fdc.load_fdc_tables import load_table_from_dump_name


def construct_temporal_trace_refined_table_from_rousset_fdc_data():
    # Load Datafiles
    MES_ASSOCIATION_TABLE_METADATA = {
        'filename': 'PCB.MESFDCAssociation.dump',
        'names': ['context_id', 'mes_context_id'],
        'types': {
            'context_id': np.int64,
            'mes_context_id': np.int64,
        },
        'null_value': 'N.A.'
    }
    VARIABLES_METADATA = {
        'filename': 'PCB.Variables.dump',
        'names': ['variable_id', 'label', 'unit', 'min_value', 'max_value', 'alias'],
        'types': {
            'variable_id': np.int64,
            'label': str,
            'unit': str,
            'min_value': np.float64,
            'max_value': np.float64,
            'alias': str
        }
    }
    MES_CONTEXT_DEFINITION_TABLE_TABLE_METADATA = {
        'filename': 'PCB.MESContextHistory.dump',
        'names': ['mes_context_id', 'technology_name', 'product_name', 'lot_name', 'wafer_name',
                  'route_name', 'stage_name', 'operation_name', 'aux1', 'aux2'],
        'types': {
            'mes_context_id': np.float64,
            'technology_name': str,
            'product_name': str,
            'lot_name': str,
            'wafer_name': str,
            'route_name': str,
            'stage_name': str,
            'operation_name': str,
            'aux1': str,
            'aux2': str
        },
        'null_value': 'N.A.'
    }
    COLLECTED_DATA_TABLE_METADATA = {
        'filename': 'PCB.CollectedDataHistory_0.dump',
        'names': ['context_id', 'variable_id', 'time_stamp', 'time_stamp_ms', 'value'],
        'types': {
            'context_id': np.float64,
            'variable_id': np.int64,
            'time_stamp': str,
            'time_stamp_ms': str,
            'value': np.float64
        }
    }
    CONTEXT_HISTORY_TABLE_METADATA = {
        'filename': 'PCB.ContextHistory.dump',
        'names': ['context_id', 'material_id', 'recipe_id', 'start_recipe_step',
                  'equipment_id', 'module_id', 'Strategy_name', 'Strategy_id',
                  'steady_event_time', 'start_event_time', 'stop_event_time',
                  'dc_quality_value', 'completed', 'is_reference', 'is_archived',
                  'archive_file', 'history_id'],
        'types': {
            'context_id': np.float64,
            'material_id': str,
            'recipe_id': str,
            'start_recipe_step': np.int64,
            'equipment_id': str,
            'module_id': str,
            'Strategy_name': str,
            'Strategy_id': str,
            'steady_event_time': str,
            'start_event_time': str,
            'stop_event_time': str,
            'dc_quality_value': np.float64,
            'completed': str,
            'is_reference': str,
            'is_archived': str,
            'archive_file': str,
            'history_id': np.int64
        }
    }

    context_history_table = load_table_from_dump_name(CONTEXT_HISTORY_TABLE_METADATA)
    mes_context_definition_table = load_table_from_dump_name(MES_CONTEXT_DEFINITION_TABLE_TABLE_METADATA)
    mes_association_table = load_table_from_dump_name(MES_ASSOCIATION_TABLE_METADATA)
    collected_data_history_table = load_table_from_dump_name(COLLECTED_DATA_TABLE_METADATA)
    variables_table = load_table_from_dump_name(VARIABLES_METADATA)

    normalized_mes_context_table = build_normalized_mes_context_table_from_mes_context(mes_context_definition_table,
                                                                                       mes_association_table)
    normalized_mes_history_context_table = merge_context_history_tables(context_history_table,
                                                                        normalized_mes_context_table)

    temporal_traces = build_temporal_traces_table(normalized_mes_history_context_table,
                                                  collected_data_history_table,
                                                  context_history_table,
                                                  variables_table)

    return temporal_traces


def construct_indicators_trend_refined_table_from_rousset_fdc_data():
    # Load Datafiles
    MES_ASSOCIATION_TABLE_METADATA = {
        'filename': 'PCB.MESFDCAssociation.dump',
        'names': ['context_id', 'mes_context_id'],
        'types': {
            'context_id': np.int64,
            'mes_context_id': np.int64,
        },
        'null_value': 'N.A.'
    }
    TREATED_DATA_HISTORY_TABLE_METADATA = {
        'filename': 'PCB.TreatedDataHistory.dump',
        'names': ['context_id', 'indicator_id', 'value'],
        'types': {
            'context_id': np.int64,
            'indicator_id': np.int64,
            'value': np.float64
        }
    }
    INDICATORS_TABLE_METADATA = {
        'filename': 'PCB.Indicators.dump',
        'names': ['indicator_id', 'strategy_id', 'label',
                  'ltrs', 'ltri', 'filter_id', 'initial_target'],
        'types': {
            'indicator_id': np.int64,
            'strategy_id': str,
            'label': str,
            'ltrs': str,
            'ltri': str,
            'filter_id': np.int64,
            'initial_target': np.float64
        }
    }
    STRATEGIES_TABLE_METADATA = {
        'filename': 'PCB.Strategies.dump',
        'names': ['strategy_id', 'label', 'dcqualitylevel', 'moduleid',
                  'dceventstemplateid', 'dceventsconfigid'],
        'types': {
            'strategy_id': str,
            'label': str,
            'dcqualitylevel': np.float64,
            'moduleid': str,
            'dceventstemplateid': str,
            'dceventsconfigid': np.int64
        }
    }
    LIMITS_TABLE_METADATA = {
        'filename': '',
        'names': '',
        'types': {
            'limit_id': np.int64,
            'indicator_id': np.int64,
            'label': str,
            'value': np.float64,
            'alarm_level': np.int64
        }
    }

    CONTEXT_HISTORY_TABLE_METADATA = {
        'filename': 'PCB.ContextHistory.dump',
        'names': ['context_id', 'material_id', 'recipe_id', 'start_recipe_step',
                  'equipment_id', 'module_id', 'Strategy_name', 'Strategy_id',
                  'steady_event_time', 'start_event_time', 'stop_event_time',
                  'dc_quality_value', 'completed', 'is_reference', 'is_archived',
                  'archive_file', 'history_id'],
        'types': {
            'context_id': np.float64,
            'material_id': str,
            'recipe_id': str,
            'start_recipe_step': np.int64,
            'equipment_id': str,
            'module_id': str,
            'Strategy_name': str,
            'Strategy_id': str,
            'steady_event_time': str,
            'start_event_time': str,
            'stop_event_time': str,
            'dc_quality_value': np.float64,
            'completed': str,
            'is_reference': str,
            'is_archived': str,
            'archive_file': str,
            'history_id': np.int64
        }
    }
    context_history_table = load_table_from_dump_name(CONTEXT_HISTORY_TABLE_METADATA)
    mes_context_definition_table = load_table_from_dump_name(TREATED_DATA_HISTORY_TABLE_METADATA)
    mes_association_table = load_table_from_dump_name(MES_ASSOCIATION_TABLE_METADATA)
    treated_data_history_table = load_table_from_dump_name(CONTEXT_HISTORY_TABLE_METADATA)
    indicators_table = merge_context_history_tables(INDICATORS_TABLE_METADATA)
    strategies_table = merge_context_history_tables(STRATEGIES_TABLE_METADATA)
    limits_table = merge_context_history_tables(LIMITS_TABLE_METADATA)

    indicators = build_indicators_trend_table(context_history_table,
                                              mes_context_definition_table,
                                              mes_association_table,
                                              treated_data_history_table,
                                              indicators_table,
                                              strategies_table,
                                              limits_table)
    return indicators
