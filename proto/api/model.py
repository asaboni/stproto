from fastapi import APIRouter, FastAPI, Response
from starlette.responses import FileResponse
import os

from proto.usecase.ingest_excel_normalisation_file import ingest_and_transform_excel_file

model_router = APIRouter(
    prefix="/model",
    responses={404: {"description": "Not found"}}
)


@model_router.get("/hello")
async def hello():
    state = "Hello_world"
    return {
        "message": str(state)
    }


@model_router.get("/excel_to_owl/")
async def excel_to_owl_new(data_source: str = "stdf_ews"):

    data = '''
     <?xml version="1.0"?>
        <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
         xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
         xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
         xmlns:owl="http://www.w3.org/2002/07/owl#"
         xml:base="http://st.com/onto.owl"
         xmlns="http://st.com/onto.owl#">"
        </rdf:RDF>
    '''
    return Response(content=data, media_type="application/xml")
