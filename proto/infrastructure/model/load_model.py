import os

import pandas as pd

NULL_CHAR = '#'


def load_sanitized_excel_file(filename):
    stdf_excel_model = pd.read_csv(filename, header='infer', sep=";").fillna(NULL_CHAR)

    return stdf_excel_model


def load_functional_flow_from_excel_file(filename):
    model_table = pd.read_excel(filename,
                                skiprows=[0],
                                sheet_name='FIELDS',
                                engine='openpyxl').fillna(NULL_CHAR)
    required_columns = ['Mandatory', 'UC', 'File_Section', 'Column_Name',
                        'Field_Content_Template_And_Rule', 'Example', 'Raw_Field_Type',
                        'Range', 'Description', 'Class', 'Sub_Class', 'Sub_Sub_Class',
                        'Class_Instance', 'Attribute_Name']
    filtered_model_table = model_table[(model_table['Mandatory'] == 'Y') &
                                       (model_table['Class_Instance'] != NULL_CHAR)][required_columns]

    header = [
        'Mandatory', 'UC', 'Record_Name', 'Column_Name', 'Template', 'Example', 'Type',
        'Range', 'Description', 'Class', 'Sub_Class', 'Sub_Sub_Class', 'Class_Instance', 'Attribute'
    ]
    filtered_model_table.columns = header

    return filtered_model_table


def generate_new_world_file(world_file):
    try:
        open(world_file)
        os.remove(world_file)
    except IOError:
        pass
    finally:
        open(world_file, 'a').close()
